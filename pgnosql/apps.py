from django.apps import AppConfig


class PgnosqlConfig(AppConfig):
    name = 'pgnosql'
