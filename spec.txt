************
TEST SUMMARY
************
-
---------------------
PGNOSQL.TESTS
---------------------
 
 GlobalUserTestCase: 
 ---------------------
  ✓ can get as a django user
  ✓ data defaults to empty dict
  ✓ get permissions
  ✓ gets data
  ✓ gets id
  ✓ gets spaces
  ✓ respects overriding user key
 
 KVTestCase: 
 ---------------------
  ✓ can delete
  ✓ get kv
  ✓ get returns none if key does not exist
  ✓ set kv
  ✓ set will update if key already exists
 
 MiddlewareTestCase: 
 ---------------------
  ✓ normal auth system works fine
